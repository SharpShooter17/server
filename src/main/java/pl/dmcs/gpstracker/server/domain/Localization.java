package pl.dmcs.gpstracker.server.domain;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;

/**
 * A Localization.
 */
@Entity
@Table(name = "localization")
public class Localization implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "acc", nullable = false)
    private String acc;

    @NotNull
    @Column(name = "alt", nullable = false)
    private String alt;

    @NotNull
    @Column(name = "bea", nullable = false)
    private Integer bea;

    @NotNull
    @Column(name = "lat", nullable = false)
    private String lat;

    @NotNull
    @Column(name = "lon", nullable = false)
    private String lon;

    @NotNull
    @Column(name = "prov", nullable = false)
    private String prov;

    @NotNull
    @Column(name = "spd", nullable = false)
    private Integer spd;

    @NotNull
    @Column(name = "sat", nullable = false)
    private Integer sat;

    @NotNull
    @Column(name = "datetime", nullable = false)
    private Instant datetime;

    @NotNull
    @Column(name = "serial", nullable = false)
    private String serial;

    @NotNull
    @Column(name = "tid", nullable = false)
    private String tid;

    @NotNull
    @Column(name = "plat", nullable = false)
    private String plat;

    @NotNull
    @Column(name = "plat_version", nullable = false)
    private String platVersion;

    @NotNull
    @Column(name = "bat", nullable = false)
    private Integer bat;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAcc() {
        return acc;
    }

    public Localization acc(String acc) {
        this.acc = acc;
        return this;
    }

    public void setAcc(String acc) {
        this.acc = acc;
    }

    public String getAlt() {
        return alt;
    }

    public Localization alt(String alt) {
        this.alt = alt;
        return this;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public Integer getBea() {
        return bea;
    }

    public Localization bea(Integer bea) {
        this.bea = bea;
        return this;
    }

    public void setBea(Integer bea) {
        this.bea = bea;
    }

    public String getLat() {
        return lat;
    }

    public Localization lat(String lat) {
        this.lat = lat;
        return this;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public Localization lon(String lon) {
        this.lon = lon;
        return this;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getProv() {
        return prov;
    }

    public Localization prov(String prov) {
        this.prov = prov;
        return this;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public Integer getSpd() {
        return spd;
    }

    public Localization spd(Integer spd) {
        this.spd = spd;
        return this;
    }

    public void setSpd(Integer spd) {
        this.spd = spd;
    }

    public Integer getSat() {
        return sat;
    }

    public Localization sat(Integer sat) {
        this.sat = sat;
        return this;
    }

    public void setSat(Integer sat) {
        this.sat = sat;
    }

    public Instant getDatetime() {
        return datetime;
    }

    public Localization datetime(Instant datetime) {
        this.datetime = datetime;
        return this;
    }

    public void setDatetime(Instant datetime) {
        this.datetime = datetime;
    }

    public String getSerial() {
        return serial;
    }

    public Localization serial(String serial) {
        this.serial = serial;
        return this;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getTid() {
        return tid;
    }

    public Localization tid(String tid) {
        this.tid = tid;
        return this;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getPlat() {
        return plat;
    }

    public Localization plat(String plat) {
        this.plat = plat;
        return this;
    }

    public void setPlat(String plat) {
        this.plat = plat;
    }

    public String getPlatVersion() {
        return platVersion;
    }

    public Localization platVersion(String platVersion) {
        this.platVersion = platVersion;
        return this;
    }

    public void setPlatVersion(String platVersion) {
        this.platVersion = platVersion;
    }

    public Integer getBat() {
        return bat;
    }

    public Localization bat(Integer bat) {
        this.bat = bat;
        return this;
    }

    public void setBat(Integer bat) {
        this.bat = bat;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if ( this == o ) {
            return true;
        }
        if ( !(o instanceof Localization) ) {
            return false;
        }
        return id != null && id.equals(((Localization) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Localization{" +
            "id=" + getId() +
            ", acc='" + getAcc() + "'" +
            ", alt='" + getAlt() + "'" +
            ", bea=" + getBea() +
            ", lat='" + getLat() + "'" +
            ", lon='" + getLon() + "'" +
            ", prov='" + getProv() + "'" +
            ", spd=" + getSpd() +
            ", sat=" + getSat() +
            ", datetime='" + getDatetime() + "'" +
            ", serial='" + getSerial() + "'" +
            ", tid='" + getTid() + "'" +
            ", plat='" + getPlat() + "'" +
            ", platVersion='" + getPlatVersion() + "'" +
            ", bat=" + getBat() +
            "}";
    }

}
