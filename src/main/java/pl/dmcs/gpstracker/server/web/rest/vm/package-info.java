/**
 * View Models used by Spring MVC REST controllers.
 */
package pl.dmcs.gpstracker.server.web.rest.vm;
