package pl.dmcs.gpstracker.server.service;

import pl.dmcs.gpstracker.server.service.dto.LocalizationDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link pl.dmcs.gpstracker.server.domain.Localization}.
 */
public interface LocalizationService {

    /**
     * Save a localization.
     *
     * @param localizationDTO the entity to save.
     * @return the persisted entity.
     */
    LocalizationDTO save(LocalizationDTO localizationDTO);

    /**
     * Get all the localizations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<LocalizationDTO> findAll(Pageable pageable);

    /**
     * Get the "id" localization.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<LocalizationDTO> findOne(Long id);

    /**
     * Delete the "id" localization.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
