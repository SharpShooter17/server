package pl.dmcs.gpstracker.server.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link pl.dmcs.gpstracker.server.domain.Config} entity.
 */
public class ConfigDTO implements Serializable {
    
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String token;

    @NotNull
    private String objectIdentifier;

    @NotNull
    private Integer posInterval;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getObjectIdentifier() {
        return objectIdentifier;
    }

    public void setObjectIdentifier(String objectIdentifier) {
        this.objectIdentifier = objectIdentifier;
    }

    public Integer getPosInterval() {
        return posInterval;
    }

    public void setPosInterval(Integer posInterval) {
        this.posInterval = posInterval;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ConfigDTO configDTO = (ConfigDTO) o;
        if (configDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), configDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ConfigDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", token='" + getToken() + "'" +
            ", objectIdentifier='" + getObjectIdentifier() + "'" +
            ", posInterval=" + getPosInterval() +
            "}";
    }
}
