package pl.dmcs.gpstracker.server.service.mapper;


import pl.dmcs.gpstracker.server.domain.*;
import pl.dmcs.gpstracker.server.service.dto.ConfigDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Config} and its DTO {@link ConfigDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ConfigMapper extends EntityMapper<ConfigDTO, Config> {



    default Config fromId(Long id) {
        if (id == null) {
            return null;
        }
        Config config = new Config();
        config.setId(id);
        return config;
    }
}
