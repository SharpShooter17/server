package pl.dmcs.gpstracker.server.service.dto;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link pl.dmcs.gpstracker.server.domain.Localization} entity.
 */
public class LocalizationDTO implements Serializable {
    
    private Long id;

    @NotNull
    private String acc;

    @NotNull
    private String alt;

    @NotNull
    private Integer bea;

    @NotNull
    private String lat;

    @NotNull
    private String lon;

    @NotNull
    private String prov;

    @NotNull
    private Integer spd;

    @NotNull
    private Integer sat;

    @NotNull
    private Instant datetime;

    @NotNull
    private String serial;

    @NotNull
    private String tid;

    @NotNull
    private String plat;

    @NotNull
    private String platVersion;

    @NotNull
    private Integer bat;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAcc() {
        return acc;
    }

    public void setAcc(String acc) {
        this.acc = acc;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public Integer getBea() {
        return bea;
    }

    public void setBea(Integer bea) {
        this.bea = bea;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public Integer getSpd() {
        return spd;
    }

    public void setSpd(Integer spd) {
        this.spd = spd;
    }

    public Integer getSat() {
        return sat;
    }

    public void setSat(Integer sat) {
        this.sat = sat;
    }

    public Instant getDatetime() {
        return datetime;
    }

    public void setDatetime(Instant datetime) {
        this.datetime = datetime;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getPlat() {
        return plat;
    }

    public void setPlat(String plat) {
        this.plat = plat;
    }

    public String getPlatVersion() {
        return platVersion;
    }

    public void setPlatVersion(String platVersion) {
        this.platVersion = platVersion;
    }

    public Integer getBat() {
        return bat;
    }

    public void setBat(Integer bat) {
        this.bat = bat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LocalizationDTO localizationDTO = (LocalizationDTO) o;
        if (localizationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), localizationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LocalizationDTO{" +
            "id=" + getId() +
            ", acc='" + getAcc() + "'" +
            ", alt='" + getAlt() + "'" +
            ", bea=" + getBea() +
            ", lat='" + getLat() + "'" +
            ", lon='" + getLon() + "'" +
            ", prov='" + getProv() + "'" +
            ", spd=" + getSpd() +
            ", sat=" + getSat() +
            ", datetime='" + getDatetime() + "'" +
            ", serial='" + getSerial() + "'" +
            ", tid='" + getTid() + "'" +
            ", plat='" + getPlat() + "'" +
            ", platVersion='" + getPlatVersion() + "'" +
            ", bat=" + getBat() +
            "}";
    }
}
