package pl.dmcs.gpstracker.server.repository;

import pl.dmcs.gpstracker.server.domain.Localization;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Localization entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LocalizationRepository extends JpaRepository<Localization, Long> {
}
