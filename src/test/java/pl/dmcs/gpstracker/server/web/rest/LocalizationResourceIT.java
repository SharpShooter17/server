package pl.dmcs.gpstracker.server.web.rest;

import pl.dmcs.gpstracker.server.GpsTrackerApp;
import pl.dmcs.gpstracker.server.domain.Localization;
import pl.dmcs.gpstracker.server.repository.LocalizationRepository;
import pl.dmcs.gpstracker.server.service.LocalizationService;
import pl.dmcs.gpstracker.server.service.dto.LocalizationDTO;
import pl.dmcs.gpstracker.server.service.mapper.LocalizationMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LocalizationResource} REST controller.
 */
@SpringBootTest(classes = GpsTrackerApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class LocalizationResourceIT {

    private static final String DEFAULT_ACC = "AAAAAAAAAA";
    private static final String UPDATED_ACC = "BBBBBBBBBB";

    private static final String DEFAULT_ALT = "AAAAAAAAAA";
    private static final String UPDATED_ALT = "BBBBBBBBBB";

    private static final Integer DEFAULT_BEA = 1;
    private static final Integer UPDATED_BEA = 2;

    private static final String DEFAULT_LAT = "AAAAAAAAAA";
    private static final String UPDATED_LAT = "BBBBBBBBBB";

    private static final String DEFAULT_LON = "AAAAAAAAAA";
    private static final String UPDATED_LON = "BBBBBBBBBB";

    private static final String DEFAULT_PROV = "AAAAAAAAAA";
    private static final String UPDATED_PROV = "BBBBBBBBBB";

    private static final Integer DEFAULT_SPD = 1;
    private static final Integer UPDATED_SPD = 2;

    private static final Integer DEFAULT_SAT = 1;
    private static final Integer UPDATED_SAT = 2;

    private static final Instant DEFAULT_DATETIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATETIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_SERIAL = "AAAAAAAAAA";
    private static final String UPDATED_SERIAL = "BBBBBBBBBB";

    private static final String DEFAULT_TID = "AAAAAAAAAA";
    private static final String UPDATED_TID = "BBBBBBBBBB";

    private static final String DEFAULT_PLAT = "AAAAAAAAAA";
    private static final String UPDATED_PLAT = "BBBBBBBBBB";

    private static final String DEFAULT_PLAT_VERSION = "AAAAAAAAAA";
    private static final String UPDATED_PLAT_VERSION = "BBBBBBBBBB";

    private static final Integer DEFAULT_BAT = 1;
    private static final Integer UPDATED_BAT = 2;

    @Autowired
    private LocalizationRepository localizationRepository;

    @Autowired
    private LocalizationMapper localizationMapper;

    @Autowired
    private LocalizationService localizationService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLocalizationMockMvc;

    private Localization localization;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Localization createEntity(EntityManager em) {
        Localization localization = new Localization()
            .acc(DEFAULT_ACC)
            .alt(DEFAULT_ALT)
            .bea(DEFAULT_BEA)
            .lat(DEFAULT_LAT)
            .lon(DEFAULT_LON)
            .prov(DEFAULT_PROV)
            .spd(DEFAULT_SPD)
            .sat(DEFAULT_SAT)
            .datetime(DEFAULT_DATETIME)
            .serial(DEFAULT_SERIAL)
            .tid(DEFAULT_TID)
            .plat(DEFAULT_PLAT)
            .platVersion(DEFAULT_PLAT_VERSION)
            .bat(DEFAULT_BAT);
        return localization;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Localization createUpdatedEntity(EntityManager em) {
        Localization localization = new Localization()
            .acc(UPDATED_ACC)
            .alt(UPDATED_ALT)
            .bea(UPDATED_BEA)
            .lat(UPDATED_LAT)
            .lon(UPDATED_LON)
            .prov(UPDATED_PROV)
            .spd(UPDATED_SPD)
            .sat(UPDATED_SAT)
            .datetime(UPDATED_DATETIME)
            .serial(UPDATED_SERIAL)
            .tid(UPDATED_TID)
            .plat(UPDATED_PLAT)
            .platVersion(UPDATED_PLAT_VERSION)
            .bat(UPDATED_BAT);
        return localization;
    }

    @BeforeEach
    public void initTest() {
        localization = createEntity(em);
    }

    @Test
    @Transactional
    public void createLocalization() throws Exception {
        int databaseSizeBeforeCreate = localizationRepository.findAll().size();

        // Create the Localization
        LocalizationDTO localizationDTO = localizationMapper.toDto(localization);
        restLocalizationMockMvc.perform(post("/api/localizations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localizationDTO)))
            .andExpect(status().isCreated());

        // Validate the Localization in the database
        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeCreate + 1);
        Localization testLocalization = localizationList.get(localizationList.size() - 1);
        assertThat(testLocalization.getAcc()).isEqualTo(DEFAULT_ACC);
        assertThat(testLocalization.getAlt()).isEqualTo(DEFAULT_ALT);
        assertThat(testLocalization.getBea()).isEqualTo(DEFAULT_BEA);
        assertThat(testLocalization.getLat()).isEqualTo(DEFAULT_LAT);
        assertThat(testLocalization.getLon()).isEqualTo(DEFAULT_LON);
        assertThat(testLocalization.getProv()).isEqualTo(DEFAULT_PROV);
        assertThat(testLocalization.getSpd()).isEqualTo(DEFAULT_SPD);
        assertThat(testLocalization.getSat()).isEqualTo(DEFAULT_SAT);
        assertThat(testLocalization.getDatetime()).isEqualTo(DEFAULT_DATETIME);
        assertThat(testLocalization.getSerial()).isEqualTo(DEFAULT_SERIAL);
        assertThat(testLocalization.getTid()).isEqualTo(DEFAULT_TID);
        assertThat(testLocalization.getPlat()).isEqualTo(DEFAULT_PLAT);
        assertThat(testLocalization.getPlatVersion()).isEqualTo(DEFAULT_PLAT_VERSION);
        assertThat(testLocalization.getBat()).isEqualTo(DEFAULT_BAT);
    }

    @Test
    @Transactional
    public void createLocalizationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = localizationRepository.findAll().size();

        // Create the Localization with an existing ID
        localization.setId(1L);
        LocalizationDTO localizationDTO = localizationMapper.toDto(localization);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLocalizationMockMvc.perform(post("/api/localizations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localizationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Localization in the database
        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkAccIsRequired() throws Exception {
        int databaseSizeBeforeTest = localizationRepository.findAll().size();
        // set the field null
        localization.setAcc(null);

        // Create the Localization, which fails.
        LocalizationDTO localizationDTO = localizationMapper.toDto(localization);

        restLocalizationMockMvc.perform(post("/api/localizations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localizationDTO)))
            .andExpect(status().isBadRequest());

        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAltIsRequired() throws Exception {
        int databaseSizeBeforeTest = localizationRepository.findAll().size();
        // set the field null
        localization.setAlt(null);

        // Create the Localization, which fails.
        LocalizationDTO localizationDTO = localizationMapper.toDto(localization);

        restLocalizationMockMvc.perform(post("/api/localizations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localizationDTO)))
            .andExpect(status().isBadRequest());

        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBeaIsRequired() throws Exception {
        int databaseSizeBeforeTest = localizationRepository.findAll().size();
        // set the field null
        localization.setBea(null);

        // Create the Localization, which fails.
        LocalizationDTO localizationDTO = localizationMapper.toDto(localization);

        restLocalizationMockMvc.perform(post("/api/localizations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localizationDTO)))
            .andExpect(status().isBadRequest());

        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLatIsRequired() throws Exception {
        int databaseSizeBeforeTest = localizationRepository.findAll().size();
        // set the field null
        localization.setLat(null);

        // Create the Localization, which fails.
        LocalizationDTO localizationDTO = localizationMapper.toDto(localization);

        restLocalizationMockMvc.perform(post("/api/localizations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localizationDTO)))
            .andExpect(status().isBadRequest());

        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLonIsRequired() throws Exception {
        int databaseSizeBeforeTest = localizationRepository.findAll().size();
        // set the field null
        localization.setLon(null);

        // Create the Localization, which fails.
        LocalizationDTO localizationDTO = localizationMapper.toDto(localization);

        restLocalizationMockMvc.perform(post("/api/localizations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localizationDTO)))
            .andExpect(status().isBadRequest());

        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkProvIsRequired() throws Exception {
        int databaseSizeBeforeTest = localizationRepository.findAll().size();
        // set the field null
        localization.setProv(null);

        // Create the Localization, which fails.
        LocalizationDTO localizationDTO = localizationMapper.toDto(localization);

        restLocalizationMockMvc.perform(post("/api/localizations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localizationDTO)))
            .andExpect(status().isBadRequest());

        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSpdIsRequired() throws Exception {
        int databaseSizeBeforeTest = localizationRepository.findAll().size();
        // set the field null
        localization.setSpd(null);

        // Create the Localization, which fails.
        LocalizationDTO localizationDTO = localizationMapper.toDto(localization);

        restLocalizationMockMvc.perform(post("/api/localizations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localizationDTO)))
            .andExpect(status().isBadRequest());

        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSatIsRequired() throws Exception {
        int databaseSizeBeforeTest = localizationRepository.findAll().size();
        // set the field null
        localization.setSat(null);

        // Create the Localization, which fails.
        LocalizationDTO localizationDTO = localizationMapper.toDto(localization);

        restLocalizationMockMvc.perform(post("/api/localizations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localizationDTO)))
            .andExpect(status().isBadRequest());

        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatetimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = localizationRepository.findAll().size();
        // set the field null
        localization.setDatetime(null);

        // Create the Localization, which fails.
        LocalizationDTO localizationDTO = localizationMapper.toDto(localization);

        restLocalizationMockMvc.perform(post("/api/localizations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localizationDTO)))
            .andExpect(status().isBadRequest());

        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSerialIsRequired() throws Exception {
        int databaseSizeBeforeTest = localizationRepository.findAll().size();
        // set the field null
        localization.setSerial(null);

        // Create the Localization, which fails.
        LocalizationDTO localizationDTO = localizationMapper.toDto(localization);

        restLocalizationMockMvc.perform(post("/api/localizations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localizationDTO)))
            .andExpect(status().isBadRequest());

        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTidIsRequired() throws Exception {
        int databaseSizeBeforeTest = localizationRepository.findAll().size();
        // set the field null
        localization.setTid(null);

        // Create the Localization, which fails.
        LocalizationDTO localizationDTO = localizationMapper.toDto(localization);

        restLocalizationMockMvc.perform(post("/api/localizations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localizationDTO)))
            .andExpect(status().isBadRequest());

        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPlatIsRequired() throws Exception {
        int databaseSizeBeforeTest = localizationRepository.findAll().size();
        // set the field null
        localization.setPlat(null);

        // Create the Localization, which fails.
        LocalizationDTO localizationDTO = localizationMapper.toDto(localization);

        restLocalizationMockMvc.perform(post("/api/localizations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localizationDTO)))
            .andExpect(status().isBadRequest());

        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPlatVersionIsRequired() throws Exception {
        int databaseSizeBeforeTest = localizationRepository.findAll().size();
        // set the field null
        localization.setPlatVersion(null);

        // Create the Localization, which fails.
        LocalizationDTO localizationDTO = localizationMapper.toDto(localization);

        restLocalizationMockMvc.perform(post("/api/localizations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localizationDTO)))
            .andExpect(status().isBadRequest());

        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBatIsRequired() throws Exception {
        int databaseSizeBeforeTest = localizationRepository.findAll().size();
        // set the field null
        localization.setBat(null);

        // Create the Localization, which fails.
        LocalizationDTO localizationDTO = localizationMapper.toDto(localization);

        restLocalizationMockMvc.perform(post("/api/localizations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localizationDTO)))
            .andExpect(status().isBadRequest());

        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLocalizations() throws Exception {
        // Initialize the database
        localizationRepository.saveAndFlush(localization);

        // Get all the localizationList
        restLocalizationMockMvc.perform(get("/api/localizations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(localization.getId().intValue())))
            .andExpect(jsonPath("$.[*].acc").value(hasItem(DEFAULT_ACC)))
            .andExpect(jsonPath("$.[*].alt").value(hasItem(DEFAULT_ALT)))
            .andExpect(jsonPath("$.[*].bea").value(hasItem(DEFAULT_BEA)))
            .andExpect(jsonPath("$.[*].lat").value(hasItem(DEFAULT_LAT)))
            .andExpect(jsonPath("$.[*].lon").value(hasItem(DEFAULT_LON)))
            .andExpect(jsonPath("$.[*].prov").value(hasItem(DEFAULT_PROV)))
            .andExpect(jsonPath("$.[*].spd").value(hasItem(DEFAULT_SPD)))
            .andExpect(jsonPath("$.[*].sat").value(hasItem(DEFAULT_SAT)))
            .andExpect(jsonPath("$.[*].datetime").value(hasItem(DEFAULT_DATETIME.toString())))
            .andExpect(jsonPath("$.[*].serial").value(hasItem(DEFAULT_SERIAL)))
            .andExpect(jsonPath("$.[*].tid").value(hasItem(DEFAULT_TID)))
            .andExpect(jsonPath("$.[*].plat").value(hasItem(DEFAULT_PLAT)))
            .andExpect(jsonPath("$.[*].platVersion").value(hasItem(DEFAULT_PLAT_VERSION)))
            .andExpect(jsonPath("$.[*].bat").value(hasItem(DEFAULT_BAT)));
    }
    
    @Test
    @Transactional
    public void getLocalization() throws Exception {
        // Initialize the database
        localizationRepository.saveAndFlush(localization);

        // Get the localization
        restLocalizationMockMvc.perform(get("/api/localizations/{id}", localization.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(localization.getId().intValue()))
            .andExpect(jsonPath("$.acc").value(DEFAULT_ACC))
            .andExpect(jsonPath("$.alt").value(DEFAULT_ALT))
            .andExpect(jsonPath("$.bea").value(DEFAULT_BEA))
            .andExpect(jsonPath("$.lat").value(DEFAULT_LAT))
            .andExpect(jsonPath("$.lon").value(DEFAULT_LON))
            .andExpect(jsonPath("$.prov").value(DEFAULT_PROV))
            .andExpect(jsonPath("$.spd").value(DEFAULT_SPD))
            .andExpect(jsonPath("$.sat").value(DEFAULT_SAT))
            .andExpect(jsonPath("$.datetime").value(DEFAULT_DATETIME.toString()))
            .andExpect(jsonPath("$.serial").value(DEFAULT_SERIAL))
            .andExpect(jsonPath("$.tid").value(DEFAULT_TID))
            .andExpect(jsonPath("$.plat").value(DEFAULT_PLAT))
            .andExpect(jsonPath("$.platVersion").value(DEFAULT_PLAT_VERSION))
            .andExpect(jsonPath("$.bat").value(DEFAULT_BAT));
    }

    @Test
    @Transactional
    public void getNonExistingLocalization() throws Exception {
        // Get the localization
        restLocalizationMockMvc.perform(get("/api/localizations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLocalization() throws Exception {
        // Initialize the database
        localizationRepository.saveAndFlush(localization);

        int databaseSizeBeforeUpdate = localizationRepository.findAll().size();

        // Update the localization
        Localization updatedLocalization = localizationRepository.findById(localization.getId()).get();
        // Disconnect from session so that the updates on updatedLocalization are not directly saved in db
        em.detach(updatedLocalization);
        updatedLocalization
            .acc(UPDATED_ACC)
            .alt(UPDATED_ALT)
            .bea(UPDATED_BEA)
            .lat(UPDATED_LAT)
            .lon(UPDATED_LON)
            .prov(UPDATED_PROV)
            .spd(UPDATED_SPD)
            .sat(UPDATED_SAT)
            .datetime(UPDATED_DATETIME)
            .serial(UPDATED_SERIAL)
            .tid(UPDATED_TID)
            .plat(UPDATED_PLAT)
            .platVersion(UPDATED_PLAT_VERSION)
            .bat(UPDATED_BAT);
        LocalizationDTO localizationDTO = localizationMapper.toDto(updatedLocalization);

        restLocalizationMockMvc.perform(put("/api/localizations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localizationDTO)))
            .andExpect(status().isOk());

        // Validate the Localization in the database
        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeUpdate);
        Localization testLocalization = localizationList.get(localizationList.size() - 1);
        assertThat(testLocalization.getAcc()).isEqualTo(UPDATED_ACC);
        assertThat(testLocalization.getAlt()).isEqualTo(UPDATED_ALT);
        assertThat(testLocalization.getBea()).isEqualTo(UPDATED_BEA);
        assertThat(testLocalization.getLat()).isEqualTo(UPDATED_LAT);
        assertThat(testLocalization.getLon()).isEqualTo(UPDATED_LON);
        assertThat(testLocalization.getProv()).isEqualTo(UPDATED_PROV);
        assertThat(testLocalization.getSpd()).isEqualTo(UPDATED_SPD);
        assertThat(testLocalization.getSat()).isEqualTo(UPDATED_SAT);
        assertThat(testLocalization.getDatetime()).isEqualTo(UPDATED_DATETIME);
        assertThat(testLocalization.getSerial()).isEqualTo(UPDATED_SERIAL);
        assertThat(testLocalization.getTid()).isEqualTo(UPDATED_TID);
        assertThat(testLocalization.getPlat()).isEqualTo(UPDATED_PLAT);
        assertThat(testLocalization.getPlatVersion()).isEqualTo(UPDATED_PLAT_VERSION);
        assertThat(testLocalization.getBat()).isEqualTo(UPDATED_BAT);
    }

    @Test
    @Transactional
    public void updateNonExistingLocalization() throws Exception {
        int databaseSizeBeforeUpdate = localizationRepository.findAll().size();

        // Create the Localization
        LocalizationDTO localizationDTO = localizationMapper.toDto(localization);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLocalizationMockMvc.perform(put("/api/localizations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localizationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Localization in the database
        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLocalization() throws Exception {
        // Initialize the database
        localizationRepository.saveAndFlush(localization);

        int databaseSizeBeforeDelete = localizationRepository.findAll().size();

        // Delete the localization
        restLocalizationMockMvc.perform(delete("/api/localizations/{id}", localization.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Localization> localizationList = localizationRepository.findAll();
        assertThat(localizationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
