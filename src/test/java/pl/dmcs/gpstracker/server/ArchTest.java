package pl.dmcs.gpstracker.server;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("pl.dmcs.gpstracker.server");

        noClasses()
            .that()
                .resideInAnyPackage("pl.dmcs.gpstracker.server.service..")
            .or()
                .resideInAnyPackage("pl.dmcs.gpstracker.server.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..pl.dmcs.gpstracker.server.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
