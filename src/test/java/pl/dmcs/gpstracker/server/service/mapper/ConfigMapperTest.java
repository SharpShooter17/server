package pl.dmcs.gpstracker.server.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ConfigMapperTest {

    private ConfigMapper configMapper;

    @BeforeEach
    public void setUp() {
        configMapper = new ConfigMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(configMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(configMapper.fromId(null)).isNull();
    }
}
